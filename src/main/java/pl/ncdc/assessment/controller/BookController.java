package pl.ncdc.assessment.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import pl.ncdc.assessment.model.Book;
import pl.ncdc.assessment.service.BookService;
import javax.validation.Valid;

//controller for communication between service and html files

@RestController
public class BookController {

    private BookService service;

    private static final Logger LOGGER = LogManager.getLogger();

    @Autowired
    public BookController(BookService service) {
        this.service = service;
    }

    @GetMapping("/")
    public ModelAndView listPage() {
        if (service.getAllBooks().isEmpty()) {
            return new ModelAndView("no-records");

        } else {
            return new ModelAndView("list-page")
                    .addObject("books", service.getAllBooks());
        }
    }

    @GetMapping("/book-form")
    public ModelAndView bookForm() {
        return new ModelAndView("book/book-form")
                .addObject("book", new Book());
    }

    @PostMapping("/add-book")
    public ModelAndView addBook(@Valid @ModelAttribute Book book, BindingResult bindingResult) {
        if (bindingResult.hasErrors()) {
            LOGGER.warn("Incorrect data input");
            return new ModelAndView("book/book-form", bindingResult.getModel());
        }
        service.addBook(book);
        LOGGER.info("Book titled " + book.getTitle() + " by " + book.getAuthor() +" has been successfully added to the databse");
        return new ModelAndView("list-page")
                .addObject("books", service.getAllBooks());
    }


}
