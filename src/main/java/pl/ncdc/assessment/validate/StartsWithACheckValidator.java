package pl.ncdc.assessment.validate;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

//class defines author validation for custom annotation
public class StartsWithACheckValidator implements ConstraintValidator<StartsWithACheck, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
       return value.startsWith("A")||value.contains(" A");
    }
}