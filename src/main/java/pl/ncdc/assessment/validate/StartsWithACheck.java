package pl.ncdc.assessment.validate;
import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

//custom annotation for author validation
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = StartsWithACheckValidator.class)

public @interface StartsWithACheck {
    String message();
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
