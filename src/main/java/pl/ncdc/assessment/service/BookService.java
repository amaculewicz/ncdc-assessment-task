package pl.ncdc.assessment.service;

import org.springframework.stereotype.Service;
import pl.ncdc.assessment.model.Book;
import pl.ncdc.assessment.repository.BookRepository;
import java.util.List;

//service for communication between app and repository
@Service
public class BookService {
    private BookRepository repository;

    public BookService(BookRepository repository) {
        this.repository = repository;
    }

    public void addBook(Book book) {
        repository.save(book);
    }

    public List<Book> getAllBooks(){
        return repository.findAll();}
}
