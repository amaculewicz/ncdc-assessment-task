package pl.ncdc.assessment.repository;

import org.springframework.data.repository.CrudRepository;
import pl.ncdc.assessment.model.Book;

import java.util.List;

public interface BookRepository extends CrudRepository<Book, Long> {
    List<Book> findAll();
}
